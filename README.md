# ewb-monitor-fw

Firmware for the EWB Monitor

## Getting started

This software is maintained by Engineers Without Borders Denmark and the
University of Southern Denmark
https://ewb-monitor.org

## Authors

Kjeld Jensen, kjen@sdu.dk, kj@iug.dk

## License

This is an open source project. Folders not listing other specific license information are released under the BSD 3-Clause license. Please see the file LICENSE for further information.

