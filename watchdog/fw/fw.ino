/***************************************************************************
EWB-Monitor (WATCHDOG) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
****************************************************************************
Board: EWB-Monitor 2023-07
Arduino compatible board: "Arduino Pro or Pro Mini"
Processor: "ATmega328P (5V, 16 MHz)"

Revision
2018-09-03 KJ First released version
2019-02-26 KJ Code cleanup. Changed sleep interval to 1s. Minor changes 
              to LED blink. Added serial debug support.
2019-08-04 KJ Changed RESET_BEFORE from 25 hours to 26 hours
2021-08-21 KJ Deleted DEBUG_ENABLE define, debug is now always on
              Disabled debug_tx() at LED blink
              Changed debug serial baud rate from 57600 to 115200
              Restructured the use of debug_tx()
              Changed initial LED blinks from 10 to 3
              Changed LED_BLINK_INTERVAL to 30 s
              Added boot signal indicating requested, power-up or forced
              reset.
              Set RESET_AFTER and RESET_BEFORE to support 22-27 hours.
              Changed reset time to 1 ms.
2023-09-16 KJ Code simplified and modified to support the 2023-07 board
              Updated from RocketScream lowpower to LowPowerLab
****************************************************************************/
#define FW_VERSION "FW 2023-09-16" 

/* parameters for min and max time before MONITOR reset [s] */
//#define RESET_AFTER  0 // used for test only
//#define RESET_BEFORE  0 // used for test only
#define RESET_AFTER  (22 * 3600UL) // min 22h before reset (due to watchdog timer inaccuracy)
#define RESET_BEFORE (27 * 3600UL) // max 27h before reset (due to watchdog timer inaccuracy)

/***************************************************************************/
/* system */
#include "ewbmon_pcb2023-07.hpp" /* EWB-Monitor board defines */
#include "LowPower.h" /* LowPowerLab (RocketScream) */ 
unsigned long time_s_approx; /* time since boot [s] */

/***************************************************************************/
/* led */
#define LED_HEARTBEAT 1 /* weak blink (1ms) */
#define LED_BLINK_INTERVAL 30 /* [s] */
unsigned long time_led_blink; /* [s] time for previous LED blink */
char i; /* iterator */

/***************************************************************************/
/* monitor communication */
#define PWRON_SIG_REQUEST_TOUT 2 /* signaling request event [s] */
#define PWRON_SIG_PWRON_TOUT 4 /* timeout for signaling power-on [s] */
#define PWRON_SIG_FORCED_RST_TOUT 6 /* timeout for signaling forced reset [s] */
unsigned long pwr_on_sig_tout;

/***************************************************************************/
void debug_tx(char *text)
{
  Serial.begin(115200);
  Serial.print (time_s_approx);
  Serial.print (" ");
  Serial.println(text);
  Serial.end();
}
/***************************************************************************/
void led_blink(char n)
{
  for (i=0; i<n; i++)
  {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    delay (LED_HEARTBEAT);
    digitalWrite(LED_BUILTIN, LOW);
    pinMode(LED_BUILTIN, INPUT);

    if (i<n-1)
      delay (300);
  }
}
/***************************************************************************/
void led_update(void)
{
  if (time_s_approx - time_led_blink >= LED_BLINK_INTERVAL)
  {
    led_blink(1);
    time_led_blink += LED_BLINK_INTERVAL;
  } 
}
/***************************************************************************/
void mon_init(void)
{
  /* send the power-up signal to the monitor */
  pwr_on_sig_tout = PWRON_SIG_PWRON_TOUT;
  debug_tx ("PWR ON");

  /* the reset request is received as a digital logic level low */
  pinMode(PIN_WD_RESET_REQ, INPUT_PULLUP);

  /* perform a boot-up reset of the MONITOR */
  mon_reset();
}
/***************************************************************************/
void mon_update(void)
{
  /* update the boot signal */
  if (time_s_approx == pwr_on_sig_tout)
  {
    /* reset the monitor signal */
    digitalWrite(PIN_WD_PWRON_SIGNAL, HIGH);
    pinMode(PIN_WD_PWRON_SIGNAL, INPUT);
    debug_tx ("SIG OFF");
  }
  
  /* check for reset request from MONITOR */
  if (digitalRead(PIN_WD_RESET_REQ) == 0)
  {
    debug_tx ("REQUEST");
    if (time_s_approx >= RESET_AFTER)
    {
      pwr_on_sig_tout = PWRON_SIG_REQUEST_TOUT;
      mon_reset();
    }
    else
    {
      debug_tx ("INVALID");    
    }
  }

  /* check if reset must be enforced */
  else if (time_s_approx >= RESET_BEFORE)
  {
    debug_tx ("FORCED");
    pwr_on_sig_tout = PWRON_SIG_FORCED_RST_TOUT;
    mon_reset();
  }
}
/***************************************************************************/
void mon_reset()
{
  /* send debug msg */
  debug_tx ("RESET");

  /* set the monitor signal output */
  pinMode(PIN_WD_PWRON_SIGNAL, OUTPUT);
  digitalWrite(PIN_WD_PWRON_SIGNAL, LOW);
  debug_tx ("SIG ON");

  /* perform the reset */
  pinMode(PIN_WD_RESET_EXEC, OUTPUT);
  digitalWrite(PIN_WD_RESET_EXEC, HIGH);
  delay (1); /* just needs to be more than microseconds */
  digitalWrite(PIN_WD_RESET_EXEC, LOW);
  pinMode(PIN_WD_RESET_EXEC, INPUT);

  /* ensure we never get a timer overrun error */
  time_s_approx = 0; 
  time_led_blink = 0;
}
/***************************************************************************/
void setup()
{
  led_blink(3); /* blink the LED a few times */
  debug_tx ("EWB Monitor (WATCHDOG)");
  debug_tx (FW_VERSION);
  mon_init(); /* initialize the monitor reset function */
  delay (2000); /* Keep the board awake a few extra secs to ease flashing */
}
/***************************************************************************/
void loop() 
{
  time_s_approx++; /* get the updated time [s] */
  led_update(); /* update the periodic LED blink */
  mon_update(); /* update the monitor reset */
    
  /* Enter power down state for 1s with ADC and BOD module disabled */
  LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);  
}
/***************************************************************************/
