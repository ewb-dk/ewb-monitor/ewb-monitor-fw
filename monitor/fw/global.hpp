/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2019-10-12 KJ First released version
2020-11-08 KJ Added link LINK_TX_INTERVAL and LINK_TX_ERR_POSTPONE
2021-08-28 KJ link_sbd.cpp and link_gsm.hpp are now seperated by #ifdefs
2023-09-16 KJ Updated to support EWB Monitor PCB 2023-07
              Added term_data()

**************************************************************************/
#ifndef GLOBAL_H
#define GLOBAL_H

/*************************************************************************/
/* firmware version */
#define FW_VERSION "2023-09-27"

/* Watchdog debug mode */
/* #define WD_DEBUG_MODE */

/* PCB  */
#include "ewbmon_pcb2023-07.hpp"

/* link (ONLY define one of the below!) */
//#define LINK_IRIDIUM /* RockBlock 9602/9603 library */
#define LINK_GSM /* SIM800L library */

/*************************************************************************/
/* includes */

#ifdef LINK_IRIDIUM
  #include "link_sbd.hpp"
#endif

#ifdef LINK_GSM
  #include "link_gsm.hpp"

  #define POST_URL_1 "http://ewb-monitor.org/mon/post.php"
  #define POST_URL_2 "http://ewbmon.dk/mon/post.php"
  #define POST_ID 202
  #define POST_TOKEN "09282023"
#endif

/*************************************************************************/
/* global variables */
extern char *serial_s; /* used for printing debug messages */

/*************************************************************************/
/* function prototypes */
extern void global_init(void);
extern void debug (char *s);
extern void term_data(char *typ, char *msg);

#endif
/*************************************************************************/
