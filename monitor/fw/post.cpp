/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2021-08-19 KJ First version
2023-09-24 KJ Updated to support EWB Monitor PCB 2023-07 header file
              Added post_read_wd_signal() in order to remove wdsig.cpp
              Changed POST_CHK_TIME to 15s and POST_LINK_CHK_TIME to 30s
              Added POST_PIEZO_DISABLE

**************************************************************************/
/* includes */
#include <Arduino.h>
#include "global.hpp"
#include "post.hpp"
#include "bat.hpp"
#include "vfs.hpp"
#include "ds18b20.hpp"

/*************************************************************************/
/* parameters */
// #define POST_PIEZO_DISABLE /* suppress piezo information */

#define POST_CHK_TIME 16000L /* time after boot [ms] (1s after batt read) */
#define POST_LINK_CHK_TIME 30000L /* time after boot [ms] */
#define POST_VFS_FLOW_ACTIVE 5 /* [liter] threshold at time of POST check */
#define PIEZO_PLAY_NOTICE 1500 /* [ms] */
#define PIEZO_PLAY_COUNT 150 /* [ms] */
#define PIEZO_PLAY_DELAY 2000 /* [ms] */
/*************************************************************************/
/* variables */
unsigned char post_check;
unsigned char post_link_check;
unsigned short post_result;

/*************************************************************************/
void post_read_wd_signal(void) {
  post_result = 0;
  pinMode(PIN_MON_PWRON_SIGNAL, INPUT_PULLUP);
  if (digitalRead(PIN_MON_PWRON_SIGNAL) == HIGH) {    
    post_result |= POST_RST_ERR;
    term_data ("RST","ERR");
  }
  else {
    delay (2000);
    if (digitalRead(PIN_MON_PWRON_SIGNAL) == HIGH) {
      /* do nothing, this means requested reset */
      term_data ("RST","REQ");
    }
    else {
      delay (2000);
      if (digitalRead(PIN_MON_PWRON_SIGNAL) == HIGH) {
        post_result |= POST_RST_POWER;
        term_data ("RST","PWR");
      }
      else {
        post_result |= POST_RST_FORCED;
        term_data ("RST","FORCED");
      }
    }
  }
}
/*************************************************************************/
static void piezo_play(char n, short ms)
{
  short i;
  
  pinMode(PIN_MON_BUZZER, OUTPUT);
  digitalWrite(PIN_MON_BUZZER, LOW);
  for (i=0; i<n; i++)
  {
    digitalWrite(PIN_MON_BUZZER, HIGH);
    delay (ms);
    digitalWrite(PIN_MON_BUZZER, LOW);
    if (i<n-1)
      delay (300);
  }
  pinMode(PIN_MON_BUZZER, INPUT);
}
/*************************************************************************/
static void signal_post_check(void)
{
  char i;

  piezo_play (1, PIEZO_PLAY_NOTICE);
  delay(PIEZO_PLAY_DELAY);

  for (i=0; i<12; i++)
  {
    if (post_result & (1<<i))
    {
      piezo_play (i+1, PIEZO_PLAY_COUNT);
      delay(PIEZO_PLAY_DELAY);      
    }   
  }
  piezo_play (1, PIEZO_PLAY_NOTICE);
}
/*************************************************************************/
static void signal_post_link_check(void)
{
  piezo_play (2, PIEZO_PLAY_NOTICE);
  delay(PIEZO_PLAY_DELAY);

  if (post_result & POST_LINK_ERR)
  {
    piezo_play (9, PIEZO_PLAY_COUNT);
    delay(PIEZO_PLAY_DELAY);
  }

  piezo_play (2, PIEZO_PLAY_NOTICE);
}
/*************************************************************************/
static void do_post_check(void)
{
  /* WD signal bits have already been set by post_read_wd_signal() */

  /* check battery */
  if (! bat_post_ok())
  {
      post_result |= POST_BAT_ERR;    
  }

  /* check flow meter */
  if (! vfs_post_ok())
  {
      post_result |= POST_FLOW_ERR;
  }

  /* check if water is running through the flow meter */
  if (vfs_vol_l >= POST_VFS_FLOW_ACTIVE)
  {
      post_result |= POST_FLOW_ACTIVE;
  }

  /* check temperature sensor */
  if (! ds18b20_temp1_post_ok())
  {
      post_result |= POST_TEMP1_ERR;
  }
 /* if (! ds18b20_temp2_post_ok())
  {
      post_result |= POST_TEMP2_ERR;
  } */

  /* print debug info */
  sprintf (serial_s, "%03x", post_result);
  term_data ("POST",serial_s);

  /* if the monitor power has been cycled */
  if (post_result & POST_RST_POWER) 
  {
    #ifndef POST_PIEZO_DISABLE
      signal_post_check();
    #endif
    link_send(false);
    link_msg_id++;
  }
}
/*************************************************************************/
void do_post_link_check(void)
{
  /* check SBD */
  if (link_result != LINK_RESULT_SEND_OK)
  {
      post_result |= POST_LINK_ERR;
  }
  
  /* print debug info */
  sprintf (serial_s, "%03x", post_result);
  term_data ("POST2",serial_s);

  /* if the monitor power has been cycled */
  if (post_result & POST_RST_POWER) 
  {
    #ifndef POST_PIEZO_DISABLE
      signal_post_link_check();
    #endif
  }
}
/*************************************************************************/
void post_init(void)
{
  post_read_wd_signal(); /* must be first */
  post_check = false;
  post_link_check = false;
}
/*************************************************************************/
void post_update(void)
{
  /* check if it is time for the POST check */
  if (post_check == false && millis() >= POST_CHK_TIME)
  {
    do_post_check();
    post_check = true;
  }

  /* check for timeout on the link check */
  if (post_result & POST_RST_POWER && post_link_check == false
    && (millis() >= POST_LINK_CHK_TIME
    || link_result == LINK_RESULT_SEND_OK || link_result == LINK_RESULT_SEND_ERR))
  {
    do_post_link_check();
    post_link_check = true;
  }
  
}
/*************************************************************************/
