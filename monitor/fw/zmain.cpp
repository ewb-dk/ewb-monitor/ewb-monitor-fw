/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2018-09-03 KJ First released version
2018-12-05 KJ Added support for an internal temperature sensor on the
              one-wire bus.
2019-04-03 KJ Fixed a bug causing continuous measurements of air temperature
              and battery voltage. Source code cleanup. Added DIP switch
              support. Added soft serial support for Iridium module. Added
              serial debug support. Reduced current consumption by only
              initializing the hw serial port prior to use and close it
              afterwards. Added 15ms idle in each loop to save further
              power. Modified LED blink.
2019-08-04 KJ Added batt_v_max. Modified DIP switch support. Added postpone
              of reset by 1 hour if the final message transmission is
              unsuccesful
2021-07-21 KJ Software restructured into different files
              Removed DIP switch functionality
              Added Watchdog signal functions
              sbdlib is no longer an external library
              Updated protocol
              Added PowerUp Self Test (POST)
              Added support for REED relay total meters
2023-09-16 KJ Updated to support EWB Monitor PCB 2023-07 header file
              Updated from RocketScream lowpower to LowPowerLab
              Moved wdsig.cpp functionality into post.cpp

**************************************************************************/
/* settings and parameters */

#define DEBUGINFO_INTERVAL 60000 /* interval between debug updates */

/*************************************************************************/
/* includes  */

#include <Arduino.h>
#include "LowPower.h" /* LowPowerLab (RocketScream) library */
#include "global.hpp"
#include "post.hpp"
#include "reset.hpp"
#include "led.hpp"
#include "bat.hpp"
#include "ds18b20.hpp"
#include "vfs.hpp"
#include "wmpulse.hpp"

/*************************************************************************/
/* defines  */

/*************************************************************************/
/* variables  */

static unsigned long prev_1s_loop;
static unsigned long prev_debuginfo;
static char power_cycle;

/*************************************************************************/
static void debuginfo_init()
{
  debug ("EWB Monitor");
  term_data ("VER",FW_VERSION);
  sprintf (serial_s, "%04d", POST_ID);
  term_data ("ID", serial_s);
}
/*************************************************************************/
static void debuginfo_update()
{
  if (link_result != LINK_RESULT_NOW_SENDING && millis()-prev_debuginfo >= DEBUGINFO_INTERVAL)
  {
    while (millis()-prev_debuginfo >= DEBUGINFO_INTERVAL)
      prev_debuginfo += DEBUGINFO_INTERVAL;
    sprintf (serial_s, "%lu,%lu.%02u", millis()/1000, vfs_vol_l, (vfs_vol_ul/10000));
    /* there appears to be an error in sprintf... so we do the %ld conversion in a seperate line */
    sprintf (serial_s + strlen(serial_s), ",%ld", vfs_temp);
    sprintf (serial_s + strlen(serial_s), ",%ld", vfs_flow_cnt/VFS_MEAS_PER_SEC);
    sprintf (serial_s + strlen(serial_s), ",%d", vfs_start_cnt);
    sprintf (serial_s + strlen(serial_s), ",%u", wmpulse_cnt);
    sprintf (serial_s + strlen(serial_s), ",%d", temp1);
    sprintf (serial_s + strlen(serial_s), ",%d", batt_v);
    term_data ("UPD",serial_s);
  }
}
/*************************************************************************/
void setup()
{
  /* disable modem 1 */
  pinMode(PIN_MON_MODEM1_EN, OUTPUT);
  digitalWrite(PIN_MON_MODEM1_EN, LOW);

  /* disable modem 2 */
  pinMode(PIN_MON_MODEM2_EN, OUTPUT);
  digitalWrite(PIN_MON_MODEM2_EN, HIGH);

  post_init(); /* initialize the Power Up Self Test (MUST be first) */
  global_init(); /* initialize global */
  led_init(); /* initialize LED */
  debuginfo_init(); /* send debug header */
  reset_init(); /* initialize the monitor controller reset */
  ds18b20_init(); /* initialize temp measurement */
  bat_init(); /* initialize the battery voltage measurement */
  vfs_init(); /* initialize vfs flow sensor measurement */
  wmpulse_init(); /* initialize water meter pulse counter */
  link_init(); /* initialize link */
  prev_1s_loop = millis();
}
/*************************************************************************/
void loop()
{
  /* 15ms loop */
  led_update(); /* update the LED */
  vfs_update(); /* update flow meter measurements */
  wmpulse_update(); /* update water meter pulse counter */
  link_update(); /* send periodical data to server */   

  /* 1s loop */
  if (millis() - prev_1s_loop >= 1000)
  {
    prev_1s_loop += 1000;
    //pwr_update();
    reset_update(); /* check if time to reset this monitor controller */
    post_update(); /* update the Power Up Self Test */
    bat_update(); /* update battery voltage measurement */
    ds18b20_update(); /* update temperature measurement */
    debuginfo_update(); /* update debug */   
  }
  
  /* sleep for 15 ms to save power but keep TIMER0 on to preserve millis() */
  if (link_result != LINK_RESULT_NOW_SENDING)
  {
    LowPower.idle(SLEEP_15MS, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_ON, SPI_OFF, USART0_OFF, TWI_OFF);
  }
}
/*************************************************************************/
