/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2018-09-03 KJ First released version pf EWB Monitor firmware
2021-07-22 KJ Moved LED functionality to separate file

**************************************************************************/
/* includes */
#include <Arduino.h>
#include "global.hpp"
#include "led.hpp"

/*************************************************************************/
/* parameters */

/*************************************************************************/
/* defines */
#define LED_BLINK_INTERVAL 15000 /* 15s */

/* variables */
static unsigned long prev_blink; /* last time the led blinked [ms] */
static unsigned char i;

/*************************************************************************/
void led_init(void)
{
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  prev_blink = millis();
}
/*************************************************************************/
void led_blink(char n, char ms)
{
  for (i=0; i<n; i++)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    delay (ms);
    digitalWrite(LED_BUILTIN, LOW);
    if (i<n-1)
      delay (300);
  }
  prev_blink = millis();
}
/*************************************************************************/
void led_update(void)
{
  if (millis()-prev_blink >= LED_BLINK_INTERVAL)
  {
    led_blink (1, LED_HEARTBEAT);
  }
}
/*************************************************************************/
