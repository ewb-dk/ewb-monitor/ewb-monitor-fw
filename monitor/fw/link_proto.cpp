/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2018-09-03 KJ First released version pf EWB Monitor firmware
2021-08-18 KJ Moved link functionality to separate file
              Updated protocol and add support for test data
2021-08-27 KJ Fixed signed char (represented as signed short by sprintf)
              and updated protocol to M6  
2021-08-28 KJ Added check for division by zero in average temperature,
              thereby fixing another signed char error. Added VFS temp max
              and updated protocol to M7  
2023-09-17 KJ Changed test_data to final_msg, added retries, added secs       
**************************************************************************/
/* includes */
#include <Arduino.h>
#include "global.hpp"
#include "link_proto.hpp"
#include "post.hpp"
#include "bat.hpp"
#include "ds18b20.hpp"
#include "vfs.hpp"
#include "wmpulse.hpp"
#ifdef LINK_IRIDIUM
  #include "link_sbd.hpp"
#endif
#ifdef LINK_GSM
  #include "link_gsm.hpp"
#endif

/*************************************************************************/
/* variables */
char link_msg[45];
static unsigned short tmp_u16;

/*************************************************************************/
void link_proto_gen_msg(unsigned char msg_id, char final_msg, unsigned char link_time, unsigned char link_rssi)
{
  char t;
  /* generate the message */
  sprintf (link_msg, "M8%02X%02X", msg_id, link_time);
  sprintf (link_msg+strlen(link_msg), "%02X%03X%02X%02X%02X", link_rssi, post_result, batt_v_min, batt_v_max, batt_v_avg_cnt/batt_v_meas_cnt);
  sprintf (link_msg+strlen(link_msg), "%02X%02X%02X", temp1_min/5, temp1_max/5, temp1_average()/5);

  /* limit measured consumption to 65000 liters */
  if (vfs_vol_l < 65000)
    tmp_u16 = vfs_vol_l;  
  else
    tmp_u16 = 65000;  
  sprintf (link_msg+strlen(link_msg), "%04X%02X%02X", tmp_u16, vfs_temp_min/5, vfs_temp_max/5);
  sprintf (link_msg+strlen(link_msg), "%04X", vfs_flow_cnt/(4*60));  /* vfs_flow_cnt is [minutes] */
  sprintf (link_msg+strlen(link_msg), "%02X", vfs_start_cnt);
  sprintf (link_msg+strlen(link_msg), "%04X", wmpulse_cnt);

  /* if not the final message, mark this in the header */
  if (! final_msg)
  {
    link_msg[0] = 'm';
  }
  debug(link_msg);
}
/*************************************************************************/
