/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
**************************************************************************/
#include "global.hpp"
#ifdef LINK_IRIDIUM

#ifndef LINK_SBD_H
#define LINK_SBD_H

/*************************************************************************/
/* defines */

#define LINK_RESULT_IDLE 0
#define LINK_RESULT_NOW_SENDING 1
#define LINK_RESULT_SEND_OK 2
#define LINK_RESULT_SEND_ERR 3

/*************************************************************************/
/* global variables */
extern unsigned char link_msg_id;
extern char link_result;
extern char link_data_sent;

/*************************************************************************/
/* function prototypes */

void link_init(void);
void link_update(void);
void link_send(char test_data);

#endif
#endif
/*************************************************************************/
