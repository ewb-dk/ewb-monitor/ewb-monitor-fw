/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2020-03-28 KJ First released version
2020-10-15 KJ Updated to support 750 ms delay before reading results (non-blocking)
2021-08-12 KJ Added ds18b20_post_ok()
2021-08-27 KJ Changed TEMP_MAX_RESET from -600 to 0 to prevent error using negative temperatures in link_proto.
2021-08-29 KJ Added temp1_avg() and temp2_avg() functions
              Changed TEMP_ERROR_VAL from -605 to 600
2023-09-25 KJ Updated to support EWB Monitor PCB 2023-07 header file
              Changed TEMP_MIN_RESET and TEMP_AVG_RESET and
              DS18_TEMP_ERROR_VAL from 60 to 100 degrees

**************************************************************************/
/* includes */
#include <Arduino.h>
#include "global.hpp"
#include "ds18b20.hpp"
#include <DallasTemperature.h>


/*************************************************************************/
/* parameters */
#define DS18_TEMP_MIN -400 /* minimum acceptable temp [degrees Celcius*10] */
#define DS18_TEMP_MAX 600 /* maximum acceptable temp [degrees Celcius*10] */
#define TEMP_MIN_RESET 1000 /* [degrees Celcius * 10] must be higher than TEMP_MAX */
#define TEMP_AVG_RESET 1000 /* [degrees Celcius * 10] */
#define TEMP_MAX_RESET 0 /* [degrees Celcius * 10] must be lower than TEMP_MIN */

/*************************************************************************/
/* defines */
#define DEBUG
#define DS18_STATE_IDLE 0
#define DS18_STATE_MEASURE 1
#define DS18_TEMP_ERROR_VAL 1000

/*************************************************************************/
/* variables */
short temp1, temp2, temp1_min, temp1_max, temp2_min, temp2_max; // temperature [degrees Celcius*10]
long temp1_avg_cnt, temp2_avg_cnt;
static short temp1_avg, temp2_avg;
static unsigned long temp1_meas_cnt, temp2_meas_cnt;
static char ds18_state;
static unsigned long ds18_next_meas, ds18_meas_tout;

OneWire oneWire(PIN_MON_TERMINAL_J);
DallasTemperature sensors(&oneWire);

/*************************************************************************/
static void temp_reset()
{
  temp1_min = TEMP_MIN_RESET;
  temp1_max = TEMP_MAX_RESET;
  temp2_min = TEMP_MIN_RESET;
  temp2_max = TEMP_MAX_RESET;
  temp1_avg_cnt = 0;
  temp2_avg_cnt = 0;
  temp1_meas_cnt = 0;
  temp2_meas_cnt = 0;
}
/*************************************************************************/
char ds18b20_temp1_post_ok(void)
{
  if (temp1 != DS18_TEMP_ERROR_VAL &&
    temp1_min > DS18_TEMP_MIN && temp1_max < DS18_TEMP_MAX)
    return true;
  else
    return false;
}
/*************************************************************************/
char ds18b20_temp2_post_ok(void)
{
  if (temp2 != DS18_TEMP_ERROR_VAL &&
    temp2_min > DS18_TEMP_MIN && temp2_max < DS18_TEMP_MAX)
    return true;
  else
    return false;
}
/*************************************************************************/
short temp1_average(void)
{
  if (temp1_meas_cnt != 0) /* prevent error if no valid DS18B20 measurement */
  {
    temp1_avg = temp1_avg_cnt/temp1_meas_cnt;
  }
  else
  {
    temp1_avg = TEMP_AVG_RESET;
  }
  return temp1_avg;  
}
/*************************************************************************/
short temp2_average(void)
{
  if (temp2_meas_cnt != 0) /* prevent error if no valid DS18B20 measurement */
  {
    temp2_avg = temp2_avg_cnt/temp2_meas_cnt;
  }
  else
  {
    temp2_avg = TEMP_AVG_RESET;
  }
  return temp2_avg;  
}
/*************************************************************************/
void ds18b20_init(void)
{
  sensors.begin(); /* Start up the dallas sensor library */
  sensors.setWaitForConversion(false); /* do not block during request */
  temp_reset();
  ds18_state = DS18_STATE_IDLE;
  ds18_next_meas = millis() + DS18B20_STARTUP;
}
/*************************************************************************/
void ds18b20_update (void)
{
  switch (ds18_state)
  {
    case DS18_STATE_IDLE:
    if (millis() >= ds18_next_meas)
    {
      /* request a new measurement */
      sensors.requestTemperatures(); // Send the command to get temperature readings
      ds18_state = DS18_STATE_MEASURE;
      ds18_meas_tout = millis() + DS18B20_MEAS_TOUT;
      ds18_next_meas += DS18B20_MEAS_INTERVAL;
    }
    break;

    case DS18_STATE_MEASURE:
      if (millis() >= ds18_meas_tout)
      {
        temp1 = (sensors.getTempCByIndex(0) * 10); /* read sensor id 0 */
  
        if (temp1 >= DS18_TEMP_MIN && temp1 <= DS18_TEMP_MAX) /* check for sensor error */
        {
          temp1_meas_cnt++;
          temp1_avg_cnt += temp1;
  
          if (temp1 < temp1_min)
            temp1_min = temp1;
          if (temp1 > temp1_max) 
            temp1_max = temp1;
        }
        else
        {
          temp1 = DS18_TEMP_ERROR_VAL;
        }
  
        temp2 = (sensors.getTempCByIndex(1) * 10); /* read sensor id 1 */
        if (temp2 >= DS18_TEMP_MIN && temp2 <= DS18_TEMP_MAX) /* check for sensor error */
        {    
          temp2_meas_cnt++;
          temp2_avg_cnt += temp2;
  
          if (temp2 < temp2_min && temp2 >= DS18_TEMP_MIN)
            temp2_min = temp2;
          if (temp2 > temp2_max && temp2 < DS18_TEMP_MAX) 
            temp2_max = temp2;
        }
        else
        {
          temp2 = DS18_TEMP_ERROR_VAL;
        }
    
        ds18_state = DS18_STATE_IDLE;
      }
    break;
  }
}
/*************************************************************************/
