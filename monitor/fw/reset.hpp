/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
**************************************************************************/
#ifndef RESET_H
#define RESET_H

/*************************************************************************/
/* function prototypes */
void reset_init(void);
void reset_update(void);

#endif
/*************************************************************************/

