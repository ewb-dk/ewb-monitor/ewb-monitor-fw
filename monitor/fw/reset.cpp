/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2018-09-03 KJ First released version pf EWB Monitor firmware
2021-08-21 KJ Moved reset functionality to separate file
2023-09-19 KJ Updated to support EWB Monitor PCB 2023-07 header file

**************************************************************************/
/* includes */
#include <Arduino.h>
#include "global.hpp"
#include "reset.hpp"

/*************************************************************************/
/* parameters */

/* time [s] for performing a reset if mgs was sent/of msg was delayed */
#ifndef WD_DEBUG_MODE
  #define MONITOR_RST_TOUT (24 * 3600000UL - 1000) /* [ms] 24h (watchdog sleeps for up to 1s) */
  #define MONITOR_RST_TOUT_POSTPONE (25 * 3600000UL - 1000) /* [ms] 25h */
#else
  #define MONITOR_RST_TOUT (15 * 60000UL - 1000) /* [ms] 15 minutes (watchdog sleeps for up to 1s) */
  #define MONITOR_RST_TOUT_POSTPONE (30 * 60000UL - 1000) /* [ms] 30 minutes */
#endif

/*************************************************************************/
void reset_init(void)
{
  pinMode(PIN_MON_RESET_REQ, OUTPUT);
  digitalWrite(PIN_MON_RESET_REQ, HIGH);
}
/*************************************************************************/
void reset_update(void)
{
  if ((link_final_msg_sent && millis() >= MONITOR_RST_TOUT) || millis() >= MONITOR_RST_TOUT_POSTPONE)
  {
    debug ("Reset");
    digitalWrite(PIN_MON_RESET_REQ, LOW);
  }
}
/*************************************************************************/
