/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
**************************************************************************/
#ifndef VFS_H
#define VFS_H

/*************************************************************************/
/* parameters */
#define VFS_5_100 /* VFS_2_40/VFS_5_100 */
#define VFS_MEAS_PER_SEC 4 /* measurements per second */

/*************************************************************************/
/* variables */
extern unsigned long vfs_vol_l; /* water volumen [liter] */
extern unsigned long vfs_vol_ul; /* water volumen [microliter] */
extern unsigned long vfs_flow_cnt; /* number of measurements with flow > 0 */
extern unsigned char vfs_start_cnt; /* number of flow starts */
extern long vfs_temp; /* water temperature [degrees*10] */
extern short vfs_temp_min; /* lowest measured water temperature [degrees*10] */
extern short vfs_temp_max; /* highest measured water temperature [degrees*10] */

/*************************************************************************/
/* function prototypes */
char vfs_post_ok(void);
void vfs_init(void);
void vfs_update (void);

#endif
/*************************************************************************/
