/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2023-07-16 KJ First released version, moved debug() from (deleted)
              debug.cpp
              Added term_data()

**************************************************************************/
/* includes */
#include <Arduino.h>
#include "global.hpp"

char *serial_s; /* allocated in global_init() */

/*************************************************************************/
void global_init(void)
{
  serial_s = (char *) malloc (70); /* this is needed! do not use serial_s[] */
}
/*************************************************************************/
void debug(char *s)
{
  Serial.begin(115200);
  Serial.print ('#');
  Serial.print (millis());
  Serial.print (" ");
  Serial.println(s);
  Serial.end();
}
/*************************************************************************/
void term_data(char *typ, char *msg)
{
  Serial.begin(115200);
  Serial.print ('$');
  Serial.print (typ);
  Serial.print (',');
  Serial.println(msg);
  Serial.end();
}
/*************************************************************************/

