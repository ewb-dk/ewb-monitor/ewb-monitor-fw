/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
**************************************************************************/
#ifndef BAT_H
#define BAT_H

/*************************************************************************/
/* parameters */
#define VOLT_MEAS_INTERVAL 15000

/*************************************************************************/
/* variables */
extern unsigned char batt_v, batt_v_min, batt_v_max; // battery voltage [V*10]
extern unsigned long batt_v_avg_cnt;
extern unsigned long batt_v_meas_cnt;

/*************************************************************************/
/* function prototypes */
char bat_post_ok(void);
void bat_init(void);
void bat_update(void);

#endif
/*************************************************************************/
