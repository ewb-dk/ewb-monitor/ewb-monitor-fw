/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
**************************************************************************/
#ifndef SBDLIB_H
#define SBDLIB_H

/*************************************************************************/
/* parameters */
/* #define DEBUG */ /* use printf for debug msgs */
#define DEBUG_SERIAL /* use sbd_debug_serial_tx() callback for debug msgs */

#define RETRY_MAX 3 /* number of times to try before giving up */

#define TIMEOUT_AT 3 /* timeout on normal AT commands */
#define TIMEOUT_CSQ 12 /* timeout on read rssi request */
#define TIMEOUT_SBDIX 60 /* timeout on send request */

/*************************************************************************/
/* sbd return codes */
#define SBD_WAIT 1
#define SBD_OK 2
#define SBD_ERR 3
#define SBD_ERR_LINK 4

/*************************************************************************/
/* library function prototypes */
void sbd_init(void);
void sbd_serial_rx_byte(unsigned char b);
void sbd_serial_rx(unsigned char *b, unsigned short b_len);

/* callback function prototypes */
extern void sbd_serial_tx(char *s);
extern void sbd_debug_serial_tx(char *s);

/* function prototypes */
char sbd_cfg(unsigned long seconds); /* configure module */

char sbd_rssi(unsigned long seconds); /* request rssi */
char sbd_rssi_result(void); /* return result */

void sbd_send_msg (char *msg); /* copy msg to send buffer */
char sbd_send(unsigned long seconds); /* send msg */
char sbd_auto_send(unsigned long seconds); /* modem_cfg & rssi before send */

/* not yet implemented */
/*
void sbd_send_bin_msg (unsigned char *msg, unsigned short len);
char sbd_send_bin(unsigned long seconds);
*/

/*************************************************************************/
#endif 

