/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2019-08-17 KJ First test using the KJ IoT Dev Swisss Army Knife
2021-08-25 KJ First weather station implementation
2021-08-28 KJ First time the library is tested using an EWB monitor
2021-08-28 KJ link_sbd.cpp and link_gsm.hpp are now seperated by #ifdefs
2023-09-25 KJ Updated to support EWB Monitor PCB 2023-07 header file
              Removed #ifdef WD_DEBUG_MODE option
              Added link_msg_id

**************************************************************************/
#include "link_gsm.hpp"
#ifdef LINK_GSM

/* parameters */
/* time [s] for sending msg/retry msg */
#define LINK_SEND_MSG_INTERVAL  (1 * 3600000UL) /* [ms] 1 hour */
#define LINK_SEND_FINAL_MSG_AT (24 * 3600000UL - LINK_SEND_TIMEOUT ) /* [ms] 24 hours */
#define LINK_SEND_FINAL_MSG_RETRY_AT (25 * 3600000UL - LINK_SEND_TIMEOUT) /* [ms] 25 hours */

#define LINK_SEND_TIMEOUT (5 * 60000UL) /* [ms] max time the modem should try sending */

/* SIM card */
#define SIM_APN "hologram"
#define SIM_PIN_ENTER false
#define SIM_PIN ""

//#define SIM_APN "telenor"
//#define SIM_PIN_ENTER false
//#define SIM_PIN "4422"

/* GSM */
#define GSM_REG_TOUT 60000UL
#define GPRS_CONN_TOUT 10000UL
#define RSSI_INVALID 0xFF

/* HTTP POST request */

#define POST_OK  "POST_OK"

#define BUF_LEN 100
/*************************************************************************/
/* includes */
#include <Arduino.h>
#include <SoftwareSerial.h>
#include "global.hpp"
#include "link_gsm.hpp"
#include "link_proto.hpp"
#include "bat.hpp"

/*************************************************************************/
/* variables */
#define GSM_STATE_DOWN 0
#define GSM_STATE_UP 1

/*************************************************************************/
/* variables */
unsigned char link_msg_id;
char link_result;
char link_final_msg_sent;

static char link_send_data_try;
static char link_send_final_msg;
static char link_msgs_sent_ok;
static char link_msgs_sent_err;

static SoftwareSerial SerialGSM(PIN_MON_MODEM1_RX, PIN_MON_MODEM1_TX);
static unsigned long link_send_prev_msg; /* time for previous interval msg send */
static unsigned long link_send_start; /* time since current transmission start [ms] */
static unsigned char len;
static char result;
static char gsm_ok;
static char gsm_state;
static char buf[BUF_LEN];
static char post_preamble[30];

/*************************************************************************/
static void gsm_send (char *data)
{
  debug (data);
  SerialGSM.print(data);  
}
/*************************************************************************/
static void gsm_send_at (char *cmd)
{
  debug (cmd);
  SerialGSM.println(cmd);  
}
/*************************************************************************/
static char gsm_get_test_response (uint32_t timeout, char *expected)
{
  buf[0] = 0;
  len = 0;
  uint32_t start = millis();
  char found_expected = false;

  while (found_expected == false && millis()- start < timeout)
  {
    while (SerialGSM.available())
    {
      buf[len++] = SerialGSM.read();
    }
    buf[len] = 0;
    if (strstr(buf, expected))
      found_expected = true;  
  }
  debug (buf);
  
  return found_expected;
}
/*************************************************************************/
static void gsm_get_response (uint32_t timeout)
{
  len = 0;
  uint32_t start = millis();
  while (millis()- start < timeout and len < BUF_LEN-1)
  {
    while (SerialGSM.available())
    {
      buf[len] = SerialGSM.read();
      buf[++len] = 0;
    }
  }
  debug (buf);
}
/*************************************************************************/
char gsm_test_response (char *expected)
{
  if (strstr(buf, expected))
    result = 1;
  else
    result = 0;
  return result; 
}
/*************************************************************************/
char gsm_registered (void)
{
  gsm_send_at("AT+CREG?");
  gsm_get_response (100);
  if (gsm_test_response (",1") or gsm_test_response (",5"))
    result = 1;
  else
    result = 0;
  return result;
}
/*************************************************************************/
char gprs_connected (void)
{
  gsm_send_at("AT+CGATT?");
  if (gsm_get_test_response (100, "+CGATT: 1"))
    result = 1;
  else
    result = 0;
  return result;
}
/*************************************************************************/
static unsigned char gsm_rssi (void)
{
  unsigned char val, rssi;
  unsigned char i;

  rssi = RSSI_INVALID;

  gsm_send_at("AT+CSQ");
  gsm_get_response (100);
  debug (buf);

  /* try to find CSQ response, e.g. "+CSQ: 15,5" */
  for (i=0; i<BUF_LEN && buf[i] != '+'; i++)
    ;

  if (i<BUF_LEN) {
    if (strlen(buf+i) > 7 && buf[i+1] == 'C'
      && buf[i+2] == 'S' && buf[i+3] == 'Q' && buf[i+4] == ':'
      && buf[i+6] >= '0' && buf[i+6] <= '9')
    {
      if (buf[i+7] >= '0' && buf[i+7] <= '9') { /* if two digits, max is 99 */
        val = (buf[i+6] - '0')*10 + (buf[i+7] - '0');
      } else { /* one digit only */
        val = (buf[i+6] - '0');
      }   
    }
    /* converstion of to RSSI based on SIM800_ATCommand Manual V1.02.pdf */
    if (val == 0) /* -115 dBm */
      rssi = 115;
    else if (val == 1)
      rssi = 111;
    else if (val > 1 && val < 31)
      rssi = 114-2*val;
    else if (val == 31)
      rssi = 52; 
  }

  return rssi;
}
/*************************************************************************/
static void link_reset (void)
{
  digitalWrite(PIN_MON_MODEM1_EN, LOW); /* turn off Modem 1 power */
  delay (100);
  digitalWrite(PIN_MON_MODEM1_EN, HIGH); /* turn on Modem 1 power */
  gsm_state = GSM_STATE_DOWN;
  debug ("Link: Reset");
}
/*************************************************************************/
static void link_off (void)
{
  digitalWrite(PIN_MON_MODEM1_EN, LOW); /* turn off Modem 1 power */
  gsm_state = GSM_STATE_DOWN;
  debug ("Link: Off");
}
/*************************************************************************/
unsigned char link_send_time (void)
{
  unsigned long diff;
  diff = (millis()-link_send_start)/1000l;
  if (diff > 255)
    diff = 255;
  return (unsigned char) diff;
}
/*************************************************************************/
void link_send(char final_msg)
{
  link_send_start = millis();
  link_result = LINK_RESULT_NOW_SENDING; /* needed because of the while condition */

  debug ("Link: Start");
  link_send_final_msg = final_msg;
  while (link_result != LINK_RESULT_SEND_OK && millis()-link_send_start <= LINK_SEND_TIMEOUT)
  {
	  link_result = LINK_RESULT_NOW_SENDING;

	  /* hardware reset the GSM modem */
	  link_reset();

	  /* make sure our serial port is up */
	  SerialGSM.begin(19200);
	  SerialGSM.flush();

	  //if (SIM_PIN_ENTER)
	  //{
  	//	gsm_send("AT+CPIN=");
  	//	gsm_send_at(SIM_PIN);
  	//	gsm_get_test_response(5000, "OK");
	  //}

	  /* wait until GSM is registered */
	  if (gsm_state != GSM_STATE_UP)
	  {
  		debug ("Link: Wait GSM");
  		uint32_t start = millis();
  		while ((!(gsm_ok = gsm_registered())) and millis()- start < GSM_REG_TOUT)
  		{
  		  //debug ("wait reg");
        delay(500);
        sprintf (serial_s, "Secs: %d", link_send_time());
        debug (serial_s);

        if (SIM_PIN_ENTER)
        {
          gsm_send("AT+CPIN=");
          gsm_send_at(SIM_PIN);
          gsm_get_test_response(5000, "OK");
        }
  		}
	  }

	  /* if GSM is registered */
	  if (gsm_ok)
	  {
	   
		/* wait until GRPS is connected */ 
		if (gsm_state != GSM_STATE_UP)
		{
		  debug ("Link: Wait GPRS");
		  uint32_t start = millis();
		  while (!(gsm_ok = gprs_connected()) and millis()- start < GPRS_CONN_TOUT)
		  {
        delay(500);
        sprintf (serial_s, "Secs: %d", link_send_time());
        debug (serial_s);
		    //debug ("wait conn");
		  }
		}


		/* if GRPS is connected */ 
		if (gsm_ok)
		{
		  debug ("Link: Sending");
		  if (gsm_state != GSM_STATE_UP)
		  {
		    gsm_send_at("ATE0"); /* HTTPREAD depends on no echo from the GSM modem */
		    gsm_get_test_response(100, "OK");                
		    gsm_send_at("AT+SAPBR=3,1,Contype,GPRS");
		    gsm_get_test_response(1000, "OK");
		    gsm_send("AT+SAPBR=3,1,APN,");
		    gsm_send_at(SIM_APN);
		    gsm_get_test_response(1000, "OK");
		    gsm_send_at("AT+SAPBR=1,1");
		    gsm_get_test_response(100, "OK");
		    gsm_send_at("AT+SAPBR=2,1");
		delay(2000); // !!!!!!!
		    gsm_get_test_response(100, "OK"); 
		    gsm_send_at("AT+HTTPINIT");
		    gsm_get_test_response(100, "OK");
		    gsm_send_at("AT+HTTPPARA=CID,1");
		    gsm_get_test_response(100, "OK");
		    SerialGSM.print("AT+HTTPPARA=URL,");
		    SerialGSM.println(POST_URL_1);
		    gsm_get_test_response(100, "OK");
		    gsm_send_at("AT+HTTPPARA=CONTENT,application/x-www-form-urlencoded");
		    gsm_ok = gsm_get_test_response(1000, "OK");
		  }

		  if (gsm_ok)
		  {
        unsigned char rssi;
        rssi = gsm_rssi();
 
        sprintf (serial_s, "Secs: %d", link_send_time());
        debug (serial_s);

        link_proto_gen_msg(link_msg_id, final_msg, link_send_time(), rssi);
        
		    sprintf (buf, "AT+HTTPDATA=%d,1000", strlen(post_preamble) + strlen(link_msg));
		    gsm_send_at(buf);

		    if (gsm_get_test_response(100, "DOWNLOAD"))
		    {
		      /* send POST parameters */
		      gsm_send(post_preamble);
		      gsm_send(link_msg);
		      gsm_get_test_response(100, "OK");

		      /* execute POST request */
		      gsm_send_at("AT+HTTPACTION=1"); /* 1 means POST action */
		      gsm_ok = gsm_get_test_response(5000, "+HTTPACTION: 1,200"); /* responds 1 for POST, 200 for OK, length, 5s is recommended */
		      //gsm_ok = gsm_get_test_response(5000, "OK"); /* responds 1 for POST, 200 for OK, length, 5s is recommended */
		      gsm_get_response(50); /* now get the buffer length even though we don't use it */

		      /* if the server responded ok to the POST request */
		      if (gsm_ok)
		      {
	  
		        gsm_send_at("AT+HTTPREAD");
		        gsm_get_response(50);            
	  
		        /* test if we have received "POST_OK" from the server */
		        // debug (buf);
		        if (strstr(buf, POST_OK) != 0)
		        {
		          link_result = LINK_RESULT_SEND_OK;
		          gsm_state = GSM_STATE_UP;
		          debug ("Link: Success");  

		          link_msgs_sent_ok++;

		          /* if this is not test data, mark data as successfully sent */
		          if (link_send_final_msg == true)
		          {
		            link_final_msg_sent = true;
		          }		        
		        }
		        else
		        {
		          link_result = LINK_RESULT_SEND_ERR;
		          gsm_state = GSM_STATE_DOWN;
		          link_msgs_sent_err++;
		          debug ("Link: Err");  
		        }
		      }
		      else
		      {
		        link_result = LINK_RESULT_SEND_ERR;
		        link_msgs_sent_err++;
		        debug("Link: POST err");
		      }
		    }
		    else
		    {
		      link_result = LINK_RESULT_SEND_ERR;
		      gsm_state = GSM_STATE_DOWN;
		      link_msgs_sent_err++;
		      debug("Link: GPRS not yet ready (DOWNLOAD)");
		    }
		  }
		  else
		  {
		    link_result = LINK_RESULT_SEND_ERR;
		    gsm_state = GSM_STATE_DOWN;
		    link_msgs_sent_err++;
		    debug("Link: GPRS not yet ready");    
		  }
		}
		else
		{
		  link_result = LINK_RESULT_SEND_ERR;
		  gsm_state = GSM_STATE_DOWN;
		  link_msgs_sent_err++;
		  debug("Link: GPRS not available");    
		}
	  }
	  else
	  {
		link_result = LINK_RESULT_SEND_ERR;
		gsm_state = GSM_STATE_DOWN;
		link_msgs_sent_err++;
		debug ("Link: GSM not registered");
	  }

	  link_off();
	  SerialGSM.end();
	}
  if (link_result == LINK_RESULT_SEND_OK)
  {
  }
}
/*************************************************************************/
void link_init()
{
  pinMode(PIN_MON_MODEM1_EN, OUTPUT);
  pinMode(PIN_MON_MODEM1_TX, OUTPUT);

  /* set status variables */
  gsm_state = GSM_STATE_DOWN;
  link_result = LINK_RESULT_IDLE;
  link_msg_id = 0;
  link_send_prev_msg = 0;
  link_send_data_try = 0;
  link_final_msg_sent = false;
  link_msgs_sent_ok = 0;
  link_msgs_sent_err = 0;


  link_off();

  sprintf (post_preamble, "id=%d&tk=%s&msg=", POST_ID, POST_TOKEN);
}
/*************************************************************************/
void link_update()
{
    /* if message transmission is active */
  if (gsm_state == GSM_STATE_UP)
  {
  }
  /* if message transmission is idle */
  else
  {
    /* if time to send a message */
    if (link_send_data_try == 0 && millis() >= LINK_SEND_FINAL_MSG_AT && batt_v > 120)
    {
      debug ("Link: final 1");
      link_send_data_try++;
      link_send(true);
      link_msg_id++;
    }
    else if (link_final_msg_sent == false && link_send_data_try == 1 && millis() >= LINK_SEND_FINAL_MSG_RETRY_AT && batt_v > 120)
    {
      debug ("Link: final 2");
      link_send_data_try++;
      link_send(true);
      link_msg_id++;
    }
    else if (millis() - link_send_prev_msg >= LINK_SEND_MSG_INTERVAL) {
      link_send(false);
      link_send_prev_msg += LINK_SEND_MSG_INTERVAL;
      link_msg_id++;
		}
  }

}
/*************************************************************************/
#endif
