/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
**************************************************************************/
#ifndef POST_H
#define POST_H

/*************************************************************************/
/* defines */

#define POST_RST_POWER 1 /* 1 */
#define POST_RST_FORCED 2 /* 2 */
#define POST_RST_ERR 4 /* 3 */
#define POST_BAT_ERR 8 /* 4 */
#define POST_FLOW_ERR 16 /* 5 */
#define POST_FLOW_ACTIVE 32 /* 6 */
#define POST_TEMP1_ERR 64 /* 7 */
#define POST_TEMP2_ERR 128 /* 8 */
#define POST_LINK_ERR 256 /* 9 */

/*************************************************************************/
/* global variables */
extern unsigned short post_result;

/*************************************************************************/
/* function prototypes */
void post_init(void);
void post_update(void);
void do_post_link_check(void);

#endif
/*************************************************************************/
