/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Board: EWB-Monitor 2023-07
Arduino compatible board: "Arduino Pro or Pro Mini"
Processor: "ATmega328P (5V, 16 MHz)"

main() is in zmain.cpp
**************************************************************************/
