/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2018-09-03 KJ First released version pf EWB Monitor firmware
2021-08-21 KJ Moved link functionality to separate file
2021-08-28 KJ link_sbd.cpp and link_gsm.hpp are now seperated by #ifdefs
2023-09-17 KJ Updated to support EWB Monitor PCB 2023-07 header file
              Removed #ifdef WD_DEBUG_MODE option
              Added link_msg_id
       
**************************************************************************/
#include "link_sbd.hpp"
#ifdef LINK_IRIDIUM

/* includes */
#include <Arduino.h>
#include <SoftwareSerial.h>
#include "global.hpp"
#include "link_proto.hpp"
#include "sbdlib.hpp"
#include "bat.hpp"

/*************************************************************************/
/* parameters */

/* time [s] for sending msg/retry msg */
#define LINK_SEND_MSG_AT (24 * 3600000UL - LINK_SEND_TIMEOUT ) /* [ms] 24 hours */
#define LINK_SEND_MSG_RETRY_AT (25 * 3600000UL - LINK_SEND_TIMEOUT) /* [ms] 25 hours */

#define LINK_SEND_TIMEOUT (6 * 60000UL) /* [ms] max time the modem should try sending */

/*************************************************************************/
/* defines */
#define LINK_STATE_IDLE 0
#define LINK_STATE_INIT 1
#define LINK_STATE_SEND 2
#define LINK_STATE_SEND_WAIT 3

#define LINK_POWER_ON_TIME 5000UL /* [ms] minimum time before the modem responds to commands*/

/*************************************************************************/
/* variables */

unsigned char link_msg_id;
char link_state;
char link_result;
char link_data_sent;

static char link_send_data_try;
static char link_send_final_msg;
static char link_msgs_sent_ok;
static char link_msgs_sent_err;

static char rssi;
static char return_code;
static char i;

static SoftwareSerial linkSS = SoftwareSerial(PIN_MON_MODEM1_RX, PIN_MON_MODEM1_TX);
static SoftwareSerial *linkSerial = &linkSS;

static unsigned long link_tx_start; /* time since current transmission start [s] */

/*************************************************************************/
static void link_power (char on)
{
  if (on)
  {
    digitalWrite(PIN_MON_MODEM1_EN, HIGH); /* turn on Modem 1 power */
    linkSerial->begin(19200); /* init the serial port */
  }
  else
  {
    linkSerial->end(); /* close the serial port */
    digitalWrite(PIN_MON_MODEM1_EN, LOW); /* turn off Modem 1 power */
  }
}
/*************************************************************************/
void link_send(char final_msg)
{
  debug ("Link: Start");
  link_send_final_msg = final_msg;

  link_result = LINK_RESULT_NOW_SENDING;
  
  /* generate message */
  link_proto_gen_msg(test_data);

  /* initialize the send message state machine */
  link_state = LINK_STATE_INIT;
}
/*************************************************************************/
static void link_state_update(void)
{
  // if any serial data available
  while (linkSerial->available() > 0)
    sbd_serial_rx_byte(linkSerial->read());

  // state machine
  switch (link_state)
  {
    case LINK_STATE_INIT:
      link_tx_start = millis();

     /* power up the iridium module */
      link_power (true);
      link_state = LINK_STATE_SEND;

    case LINK_STATE_SEND:
      if (millis()-link_tx_start >= LINK_POWER_ON_TIME)
      {     
        sbd_init(); // initialize Short Burst Data library
        sbd_send_msg (link_msg);
        link_state = LINK_STATE_SEND_WAIT;
      }
      break;

    case LINK_STATE_SEND_WAIT:
      /* Update SBD send msg */
      return_code = sbd_auto_send(millis()/1000);

      /* check the return code */
      if (return_code != SBD_WAIT)
      {
        /* if the message was sent successfully */
        if (return_code == SBD_OK)
        {
          link_power (false);
          debug ("Link: Success");
          link_state = LINK_STATE_IDLE;
          link_result = LINK_RESULT_SEND_OK;
          link_msgs_sent_ok++;

          /* if this is not test data, mark data as successfully sent */
          if (link_send_test_data == false)
          {
            link_data_sent = true;
          }
        }
        else
        {
          if (millis()-link_tx_start >= LINK_SEND_TIMEOUT)
          {
            link_power (false);
            debug ("Link: Error");
            // led_blink(3, LED_SIGNAL);
            link_result = LINK_RESULT_SEND_ERR;
            link_state = LINK_STATE_IDLE;
            link_msgs_sent_err++;
          }
          else          
          {
            debug ("Link: Retry");
            link_state = LINK_STATE_SEND;
          }
        }
      }
      break;
  }
}
/*************************************************************************/
void link_init(void)
{
  link_power (false); /* put the RockBlock in sleep mode (while still conf. as input) */
  pinMode(PIN_MON_MODEM1_EN, OUTPUT); /* configure the Modem 1 power pin as output */

  /* set status variables */
  link_state = LINK_STATE_IDLE;
  link_result = LINK_RESULT_IDLE;
  link_send_data_try = 0;
  link_data_sent = false;
  link_msgs_sent_ok = 0;
  link_msgs_sent_err = 0;
}
/*************************************************************************/
void link_update()
{
  /* if message transmission is active */
  if (link_state != LINK_STATE_IDLE)
  {
    link_state_update();
  }
  /* if message transmission is idle */
  else
  {
    /* if time to send a message */
    if (link_send_data_try == 0 && millis() >= LINK_SEND_MSG_AT && batt_v > 120)
    {
      debug ("Link: 1");
      link_send_data_try++;
      link_msg_id++;
      link_send(false);
    }
    else if (link_data_sent == false && link_send_data_try == 1 && millis() >= LINK_SEND_MSG_RETRY_AT && batt_v > 120)
    {
      debug ("Link: 2");
      link_send_data_try++;
      link_msg_id++;
      link_send(false);
    }
  }
}
/*************************************************************************/
/* SBD library serial tx callback function */
void sbd_serial_tx(char *s)
{
  linkSerial->print (s);
}
/*************************************************************************/
/* SBD library debug serial tx callback function */
void sbd_debug_serial_tx(char *text)
{
  debug(text);
}
/*************************************************************************/
#endif
