/**************************************************************************
EWB-Monitor 2023-07 Board header file
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Board: EWB-Monitor 2023-07
Arduino compatible board: "Arduino Pro or Pro Mini"
Processor: "ATmega328P (5V, 16 MHz)"

Revision
2023-09-16 KJ First released version
**************************************************************************/
#ifndef EWBMON_PCB_H
#define EWBMON_PCB_H

#define BOARD_EWBMON_PCB2023-07

/*************************************************************************/
// WatchDog (WD)
#define PIN_WD_RESET_REQ  A4 // PC4/SDA connected to MON (PC4/SDA)
#define PIN_WD_PWRON_SIGNAL  A5 // PC5/SCL connected to MON (PC5/SCL)
#define PIN_WD_RESET_EXEC  7 // PD7 connected to MON (RESET)

/*************************************************************************/
// Monitor (MON)
#define PIN_MON_BUZZER  6 // PD6
#define PIN_MON_VBATT  A0 // PC0/ADC0

#define PIN_MON_RESET_REQ  A4 // PC4/SDA connected to WD (PC4/SDA)
#define PIN_MON_PWRON_SIGNAL  A5 // PC5/SCL connected to WD (PC5/SCL)

#define PIN_MON_MODEM1_EN  4 // PD4 (Modem1 +5VDC, 2.5A, requires JP2 short)
#define PIN_MON_MODEM1_RX  11 // PB3/MOSI
#define PIN_MON_MODEM1_TX  12 // PB4/MISO

#define PIN_MON_MODEM2_EN  5 // PD5 (250mA shared with AUX1, requires JP3 short)
#define PIN_MON_MODEM2_RX  9 // PB1
#define PIN_MON_MODEM2_TX  10 // PB2

#define PIN_MON_AUX1_EN  7 // PD7 (AUX1 VCC, 250mA shared with Modem2)

/*************************************************************************/
// Screw terminals
// TERMINAL_A   GND
// TERMINAL_B   VIN
// TERMINAL_C   RX_MON (PD0/RXD)
// TERMINAL_D   TX_MON (PD1/TXD)
// TERMINAL_E   GND
// TERMINAL_F   VCC
#define PIN_MON_TERMINAL_G  2 // GPIO_D1/PD2/INT0
#define PIN_MON_TERMINAL_H  3 // GPIO_D2/PD3/INT1

#define PIN_MON_TERMINAL_I  8 // PB0
#define PIN_MON_TERMINAL_J  A3 // PC3 (1-WIRE if JP4 is shorted)
// TERMINAL_K   GND
// TERMINAL_L   VCC
#define PIN_MON_TERMINAL_M  A2 // PC2
#define PIN_MON_TERMINAL_N  A1 // PC1
// TERMINAL_O   GND
// TERMINAL_P   GND
// TERMINAL_Q	AUX1 VCC

/*************************************************************************/
#endif
