/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
**************************************************************************/
#ifndef LED_H
#define LED_H

/*************************************************************************/
/* defines */
#define LED_HEARTBEAT 1 /* weak blink (1ms) */
#define LED_SIGNAL 100 /* powerful blink (100ms) */

/*************************************************************************/
/* function prototypes */
void led_init(void);
void led_blink(char n, char ms);
void led_update(void);

#endif
/*************************************************************************/
