/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Revision
2018-09-03 KJ First released version pf EWB Monitor firmware
2021-08-12 KJ Moved voltage meassurement to separate file
2023-09-06 KJ Updated to support EWB Monitor PCB 2023-07 header file
              Modified parameters for voltage measurement 

**************************************************************************/
/* includes */
#include <Arduino.h>
#include "global.hpp"
#include "bat.hpp"

/*************************************************************************/
/* defines */
#define BAT_POST_V_MIN 124 /* [Volt*10] */
#define BAT_POST_V_MAX 150 /* [Volt*10] */

/*************************************************************************/
/* global variables */
unsigned char batt_v, batt_v_min, batt_v_max; // battery voltage [V*10]
unsigned long batt_v_avg_cnt;
unsigned long batt_v_meas_cnt;
/*************************************************************************/
/* static variables */
static unsigned long prev_volt_meas;

/*************************************************************************/
char bat_post_ok(void)
{
  if (batt_v_min >= BAT_POST_V_MIN && batt_v_max <= BAT_POST_V_MAX)
    return true;
  else
    return false;
}
/*************************************************************************/
void bat_init(void)
{
  batt_v_min = 255;
  batt_v_max = 0;
  batt_v_avg_cnt = 0;
  batt_v_meas_cnt = 0;
  prev_volt_meas = 0;
}
/*************************************************************************/
void bat_update (void)
{ 
  if (millis()-prev_volt_meas >= VOLT_MEAS_INTERVAL)
  {
    prev_volt_meas += VOLT_MEAS_INTERVAL;

    // see ewb_mon1_batt_volt.ino for documentation
    batt_v = (((analogRead(PIN_MON_VBATT) * 320000L) >> 10) + 500) /1000;

    batt_v_avg_cnt += batt_v;
    batt_v_meas_cnt++;

    if (batt_v_min > batt_v)
      batt_v_min = batt_v;
    if (batt_v_max < batt_v)
      batt_v_max = batt_v;
  }
}
/*************************************************************************/

