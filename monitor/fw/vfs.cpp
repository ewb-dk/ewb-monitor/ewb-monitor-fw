/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Grundfos VFS flow meter measurement

Revision
2018-09-03 KJ First released version of EWB Monitor firmware
2021-08-12 KJ Moved voltage meassurement to separate file
2021-08-28 KJ Changed VFS_TEMP_MIN_RESET from 800 to 600 and
              VFS_TEMP_MAX_RESET from -600 to 0 for compliance with
              DS18B20 temp.
              Fixed vfs_temp_min error due to startup before the VFS is
              ready  
2021-09-03 KJ Changed VFS startup time from 500ms to 1000ms              
2023-09-25 KJ Updated to support EWB Monitor PCB 2023-07 header file
              Changed VFS_TEMP_MIN_RESET to 100
              Changed VFS_FLOW_THRESHOLD to 10 sek
              Changed VFS_NOFLOW_THRESHOLD to 10 sek

**************************************************************************/
/* includes */
#include <Arduino.h>
#include "global.hpp"
#include "vfs.hpp"

/*************************************************************************/
/* defines */

/* #define DEBUG */

#define VFS_ADC_THRESHOLD_LOW 102  /* adc value corresponding to 0.5 Volt */
#define VFS_ADC_THRESHOLD_HIGH 717  /* adc value corresponding to 3.5 Volt */

        /* VFS 2-40 l/m */
        /* calculation error is approx 0.05l/m at 40 l/m */
        /* 2l/min = 102.4 [adc], 40l/min = 716.8 [adc]
           a = (40-2)*1e6/(716.8-102.4) = +61849
           b = 2000000 - 61849*102.4 = -4333338 */
#ifdef VFS_2_40
#define VFS_FLOW_A 61849L
#define VFS_FLOW_B -4333338L
#endif

        /* VFS 5-100 l/m */
        /* calculation error is approx 0.14l/m at 100 l/m */
        /* 5l/min = 102.4 [adc], 100l/min = 716.8 [adc]
           a = (100-5)*1e6/(716.8-102.4) = +154622
           b = 5000000 - 154622*102.4 = -10833293 */
#ifdef VFS_5_100
#define VFS_FLOW_A 154622L
#define VFS_FLOW_B -10833293L
#endif

        /* VFS temperature */
        /* 0 deg = 102.4 [adc], 100 deg = 716.8 [adc]
           a = (100-0)*1e6/(716.8-102.4) = +162760
           b =  0 - 162760*102.4 = -16666667 */
#define VFS_TEMP_A 162920L
#define VFS_TEMP_B -16666667L
#define VFS_TEMP_MIN_RESET 1000 /* Degrees Celcius * 10] */
#define VFS_TEMP_MAX_RESET 0 /* Degrees Celcius * 10] */

#define VFS_STARTUP 1000 /* time before VFS is ready [ms] */
#define VFS_MEAS_INTERVAL (1000 / VFS_MEAS_PER_SEC) /* interval between measurements [ms] */
#define VFS_MEAS_PER_MIN (60 * VFS_MEAS_PER_SEC) /* # measurements/min */

#define VFS_POST_FLOW_MIN  20 /* [0;1023] ADC value: 20 ~ 0.1 Volt */
#define VFS_POST_FLOW_MAX 717 /* [0;1023] ADC value: 717 ~ 3.5 Volt */
#define VFS_POST_TEMP_MIN 102 /* [0;1023] ADC value: 102 ~ 0.5 Volt */
#define VFS_POST_TEMP_MAX 717 /* [0;1023] ADC value: 717 ~ 3.5 Volt */

#define VFS_FLOW_THRESHOLD (10*VFS_MEAS_PER_SEC) /* [s/4] number of measurements of positive flow before acknowledging start */
#define VFS_NOFLOW_THRESHOLD (10*VFS_MEAS_PER_SEC) /* [s/4] number of no flow measurements before acknowledging a new start */

/*************************************************************************/
/* global variables */
unsigned long vfs_vol_l; /* water volumen [liter] */
unsigned long vfs_vol_ul; /* water volumen [microliter] */
unsigned long vfs_flow_cnt; /* number of measurements with flow > 0 */
unsigned char vfs_start_cnt; /* number of flow start and stop */
long vfs_temp; /* water temperature [degrees*10] */
short vfs_temp_min; /* lowest measured water temperature [degrees*10] */
short vfs_temp_max; /* highest measured water temperature [degrees*10] */

/*************************************************************************/
/* static variables */
static unsigned short vfs_flow_adc; /* water flow [adc/min] */
static unsigned long vfs_flow_ul_m; /* water flow [microliter/min] */
static unsigned long vfs_vol_meas_ul; /* water volumen for this measurement [microliter] */
static unsigned short vfs_temp_adc; /* water temperature [adc] */
static unsigned long time_vfs_ready;  /* time the VFS is ready [ms] */
static unsigned long time_vfs_prev_meas;  /* time since boot to previous measurement [ms] */
static unsigned long time_prev_status;  /* time since boot to previous status update [ms] */
static unsigned long vfs_flow_yes; /* latest count of measurements with flow */
static unsigned long vfs_flow_no; /* latest count of measurements without flow */
static unsigned char vfs_isflow; /* is there a flow now or not */
/*************************************************************************/
void vfs_init(void)
{
  /* power on (and reset) the vfs flow sensor */
  pinMode(PIN_MON_AUX1_EN, OUTPUT);
  digitalWrite(PIN_MON_AUX1_EN, HIGH);
  delay (100);
  digitalWrite(PIN_MON_AUX1_EN, LOW);
  time_vfs_ready = millis() + VFS_STARTUP;

  time_vfs_prev_meas = 0;
  vfs_vol_ul = 0;
  vfs_vol_l = 0;
  vfs_flow_cnt = 0;
  vfs_flow_yes = 0;
  vfs_flow_no = 0;
  vfs_isflow = false;
  vfs_temp_min = VFS_TEMP_MIN_RESET;
  vfs_temp_max = VFS_TEMP_MAX_RESET;
}
/*************************************************************************/
char vfs_post_ok(void)
{
  vfs_flow_adc = analogRead(PIN_MON_TERMINAL_N);
  vfs_temp_adc = analogRead(PIN_MON_TERMINAL_M);

  if (vfs_flow_adc >= VFS_POST_FLOW_MIN && vfs_flow_adc <= VFS_POST_FLOW_MAX
    && vfs_temp_adc >= VFS_POST_TEMP_MIN && vfs_temp_adc <= VFS_POST_TEMP_MAX)
  {
    return true;
  }
  else
  {
    return false;
  }
}
/*************************************************************************/
static void vfs_meas(void)
{
  /* read vfs flow sensor */
  vfs_flow_adc = analogRead(PIN_MON_TERMINAL_N);
  /*vfs_flow_adc = VFS_ADC_THRESHOLD_LOW; */  /* used when testing calculations */

  /* check if within thresholds (function is undefined outside) */
  if (vfs_flow_adc < VFS_ADC_THRESHOLD_LOW)
    vfs_flow_adc = 0;
  else if (vfs_flow_adc > VFS_ADC_THRESHOLD_HIGH)
    vfs_flow_adc = VFS_ADC_THRESHOLD_HIGH;

  /* if there is a flow */
  if (vfs_flow_adc > 0)
  {
    vfs_flow_cnt++;
    vfs_flow_yes++;
    vfs_flow_no = 0;
    
    /* determine flow in microliters/minute */
    vfs_flow_ul_m = vfs_flow_adc * VFS_FLOW_A + VFS_FLOW_B;

    /* integrate flow to add volume since last measurement */ 
    vfs_vol_meas_ul =  vfs_flow_ul_m / 60 * (millis() - time_vfs_prev_meas) / 1000;
    time_vfs_prev_meas = millis(); /* update measurement time variable */

    vfs_vol_ul += vfs_vol_meas_ul;

    /* if more than 1 liter then update the liter counter */
    while (vfs_vol_ul > 1000000)
    {
      vfs_vol_l += 1;
      vfs_vol_ul -= 1000000;
    }
  }
  else
  {
    vfs_flow_ul_m = 0;
    vfs_flow_no++;
    vfs_flow_yes = 0;
  }

  /* flow detection housekeeping */
  if (vfs_isflow == false)
  {
    if (vfs_flow_yes >= VFS_FLOW_THRESHOLD)
    {
      vfs_isflow = true;
      if (vfs_start_cnt < 255)
      {
        vfs_start_cnt++;
      }
    } 
  }
  else
  {
    if (vfs_flow_no >= VFS_NOFLOW_THRESHOLD)
    {
      vfs_isflow = false;
    } 
  }

  /* read vfs temperature sensor (analog [0;1023]) */
  vfs_temp_adc = analogRead(PIN_MON_TERMINAL_M);

  if (vfs_temp_adc < VFS_ADC_THRESHOLD_LOW)
  {
    vfs_temp_adc = VFS_ADC_THRESHOLD_LOW;
  }
  /* vfs_temp_adc = VFS_ADC_THRESHOLD_HIGH; */ /* used when testing calculations */

  /* calculate temperature in degrees */
  vfs_temp = (long) vfs_temp_adc * VFS_TEMP_A + VFS_TEMP_B;
  vfs_temp /= 100000;

  /* update minimum/maximum values */
  if (vfs_temp_adc > VFS_ADC_THRESHOLD_LOW)
  {
    if (vfs_temp < vfs_temp_min)
      vfs_temp_min = vfs_temp;
    if (vfs_temp > vfs_temp_max)
      vfs_temp_max = vfs_temp;
  }
}
/*************************************************************************/
void vfs_update (void)
{
  /* update the time */
  if (millis() - time_vfs_prev_meas >= VFS_MEAS_INTERVAL && millis() > time_vfs_ready)
  {
    vfs_meas();
  }
}
/*************************************************************************/
