/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
**************************************************************************/
#ifndef DS18B20_H
#define DS18B20_H

/*************************************************************************/
/* parameters */
#define DS18B20_STARTUP 500 /* time before DS18B20 is ready [ms] */
#define DS18B20_MEAS_INTERVAL 15000 /* interval between measurements [ms] */
#define DS18B20_MEAS_TOUT 800 /* time from request to data ready */
/*************************************************************************/
/* variables */

extern short temp1, temp2, temp1_min, temp1_max, temp2_min, temp2_max; // temperature [degrees Celcius*10]
extern long temp1_avg_cnt, temp2_avg_cnt;
extern unsigned long temp1_meas_cnt, temp2_meas_cnt;

/*************************************************************************/
/* function prototypes */
char ds18b20_temp1_post_ok(void);
char ds18b20_temp2_post_ok(void);
short temp1_average(void);
short temp2_average(void);
void ds18b20_init(void);
void ds18b20_update (void);

#endif
/*************************************************************************/
