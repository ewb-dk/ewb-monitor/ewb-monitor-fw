/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
***************************************************************************
Water meter pulse measurement

Revision
2021-07-21 KJ First released version
2023-09-06 KJ Updated to support EWB Monitor PCB 2023-07 header file

**************************************************************************/
/* includes */
#include <Arduino.h>
#include "global.hpp"
#include "wmpulse.hpp"

/*************************************************************************/
/* defines */
#define DEBOUNCE_TIME 500 /* [ms] */

#define DEBOUNCE_STATE_IDLE 0
#define DEBOUNCE_STATE_NEW_INT 1
#define DEBOUNCE_STATE_ACTIVE 2

/*************************************************************************/
/* global variables */
volatile unsigned short wmpulse_cnt;  /* number of water meter pulses */
/*************************************************************************/
/* static variables */
static volatile char debounce;
static unsigned long time_last_pulse;  /* time since boot to previous measurement [ms] */

/*************************************************************************/
static void wmpulse_interrupt(void)
{
  if (debounce == DEBOUNCE_STATE_IDLE)
  {
    wmpulse_cnt++;
    debounce = DEBOUNCE_STATE_NEW_INT;
  }
}
/*************************************************************************/
void wmpulse_init(void)
{
  pinMode(PIN_MON_TERMINAL_H, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PIN_MON_TERMINAL_H),wmpulse_interrupt, FALLING); 
  wmpulse_cnt = 0;
  debounce =  DEBOUNCE_STATE_IDLE;
}
/*************************************************************************/
void wmpulse_update (void)
{
  if (debounce ==  DEBOUNCE_STATE_NEW_INT)
  {
    time_last_pulse = millis();
    debounce = DEBOUNCE_STATE_ACTIVE;
  }
  else if (debounce == DEBOUNCE_STATE_ACTIVE)
  {
    if (millis() - time_last_pulse >= DEBOUNCE_TIME)
    {
      debounce = DEBOUNCE_STATE_IDLE;
    }
  }
}
/*************************************************************************/

