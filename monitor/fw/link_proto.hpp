/**************************************************************************
EWB-Monitor (MONITOR) Firmware
Copyright (c) 2018-2023 Kjeld Jensen <kj@kjen.dk>
Use is subject to license conditions specified in the BSD 3-Clause license
Please see the file LICENSE for further information
**************************************************************************/
#ifndef LINK_PROTO_H
#define LINK_PROTO_H

/*************************************************************************/
/* parameters */

/*************************************************************************/
/* defines */

/**************************************************************************/
/* global variables */
extern char link_msg[45];

/**************************************************************************/
/* function prototypes */

void link_proto_gen_msg(unsigned char msg_id, char final_msg, unsigned char link_secs, unsigned char link_rssi);

#endif
/**************************************************************************/
